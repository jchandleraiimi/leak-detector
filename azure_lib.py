from sqlalchemy import create_engine
import urllib
import pandas as pd
from turbodbc import connect as turbocon
import pyarrow
import math
from multiprocessing.pool import ThreadPool as ThreadPool

try:
    import config as c
except ImportError:
    raise ImportError('''Your username and password needs to be entered in a seperate config.py and added to your python path. 
                          This config file should be stored in a secure location, with the variables UID and PWD
                          UID = KEVIN@anglianwater.co.uk 
                          PWD = 1L0v3pu8qu1zz3s''')

DRIVER = r'{ODBC Driver 13 for SQL Server}'
SERVER = r'aws-insights-sqlsrv-01.database.windows.net'
DATABASE = r'aws-insights-sqldb-01'
ATH_TYPE = 'ActiveDirectoryPassword'
UID = c.UID 
PWD = c.PWD

#get turbodbc connection
def get_turbo_con():
    
    con = turbocon(driver=DRIVER,
                      server=SERVER,
                      database=DATABASE,
                      Authentication=ATH_TYPE,
                      uid=UID,
                      pwd=PWD)
    return con
    

#get sql alchemy connection string
def get_sqlalchemy_constr():
    quoted_conn_str = urllib.parse.quote_plus(f'DRIVER={DRIVER};SERVER={SERVER};DATABASE={DATABASE};Authentication={ATH_TYPE};UID={UID};PWD={PWD}')
    return 'mssql+pyodbc:///?odbc_connect={}'.format(quoted_conn_str)

#get a sql alchemy engine
#echo turns on logging
def get_engine(echo=False):
    return create_engine(get_sqlalchemy_constr(), echo=echo)

#executes a sql command
def execute_sql(SQL, engine=None):
    if engine == None:
        engine = get_engine()
       
    with engine.connect() as con:
        con.execute(SQL)

#select query, returns data as dataframe, list or cursor
def select_sql(SQL, engine=None, return_type='df'): 
    ALLOWED_TYPES = {'df','list','cursor'}
    
    if return_type not in ALLOWED_TYPES:
         raise ValueError("return_type must be one of " + list(ALLOWED_TYPES) + f' not {return_type}') 
    
    if engine == None:
        engine = get_engine()
    
    with engine.connect() as con:
        if return_type =='df':
            df = pd.read_sql(SQL,engine)
            return df
        
        cur = con.execute(SQL)
        
        if return_type == 'cursor':
            return cur
        
        #TODO This returns list for each row, should it be list for each column?
        if return_type == 'list':
            rs = cur.fetchall()
    
            result = [cur.keys()]
                
            for r in rs:
                result += [list(r)]
        
            return result

#function to append data frame to table via turbodbc
def __exec_append(df, columns, turbo_con, schema, table):
    pt = pyarrow.Table.from_pandas(df[columns], preserve_index =False)
        
    data_params = '?, '*(len(columns)-1) + '?'
    data_coln = ','.join(columns)
    
    sql = f'INSERT INTO [{schema}].[{table}] ({data_coln}) VALUES ({data_params})'
    
    cursor = turbo_con.cursor()
    
    cursor.executemanycolumns(sql, pt) 
    
    #turbo_con.commit()

#function to update data in dataframe to table via turbodbc
def __exec_update(df, columns, match_cols, turbo_con, schema, table):
    pt = pyarrow.Table.from_pandas(df[columns + match_cols], preserve_index =False)
    data_coln = '[' + '] = ?,['.join(columns) + '] = ?'
    match_coln = '[' + '] = ?,['.join(match_cols) + '] = ?'
    
    sql = f'UPDATE [{schema}].[{table}] SET {data_coln} WHERE {match_coln}'
    
    cursor = turbo_con.cursor()
    
    cursor.executemanycolumns(sql, pt) 
    
    #turbo_con.commit()


          
#main function for importing large pandas dataframes to sql quickly
def pandas_to_sql(df, schema, table, engine=None, operation='append',columns=None, match_cols=None, threads=1, skip_exists_check = False):
    
    ALLOWED_OPS = {'update','append'}
    
    if operation not in ALLOWED_OPS:
        raise ValueError("operation must be one of " + list(ALLOWED_OPS) + f' not {operation}') 
    
    if operation == 'update' and match_cols is None:
        raise ValueError('if operation is update then match_cols must have values') 
        

    if not skip_exists_check:
        #check if table exists
        sql_check_table = """SELECT count(TABLE_NAME) texists
                             FROM INFORMATION_SCHEMA.TABLES 
                             WHERE 
                    				 TABLE_SCHEMA = '{0}' 
                                 AND  TABLE_NAME = '{1}'""".format(schema,table)
        
    
        test_cur = get_turbo_con().cursor().execute(sql_check_table)
        
        exists = test_cur.fetchone()
        
        
        if exists[0] == 0:
            #table does not exist
            if operation == 'update':
                #throw exception as table needs to exist to do an update
                raise ValueError(f'The table {schema}.{table} does not exist')
            
            elif operation == 'append':
                #create table to append to
                if engine is None:
                    engine = get_engine()
                    
                df.iloc[:0,:].to_sql(table, engine, schema = schema, index=False)
        
     
    #get speedy connection 
    turbo_con = get_turbo_con()

    
    #if columns is undefined set equal to all columns not matched
    if columns is None:
        if operation == 'update':
            columns = list(set(df.columns) - set(match_cols))
        
        elif operation == 'append':
            columns = list(df.columns)
    

    #to not effect old code    
    if threads == 1:
        
        if  operation == 'update':
            __exec_update(df, columns, match_cols, turbo_con, schema, table)
            
        elif operation == 'append':
            __exec_append(df, columns, turbo_con, schema, table)
        
    #run multi threaded version
    else:
        tot_rows = len(df.index)
        rows_per_thread = math.ceil(tot_rows / threads)
        row = 0
        
       
        with ThreadPool(processes=threads) as pool:
            # First create my processes
            async_processes = {}
            for i in range(threads):
                #split dataframe into sections
                df_work = df.iloc[(row+(rows_per_thread*i)):min(rows_per_thread*(i+1),tot_rows),:]
                
                #run functions
                if  operation == 'update':           
                    async_processes[i] = pool.apply_async(__exec_update, (df_work, columns, match_cols, turbo_con, schema, table))
                    
                elif operation == 'append':
                    async_processes[i] = pool.apply_async(__exec_append, (df_work, columns, turbo_con, schema, table))  
               
                
            # Now await their results
            for i, res in async_processes.items():
                res.get()
            
    #commit
    turbo_con.commit()

            
        

    




