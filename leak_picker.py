from bokeh.embed import components
from bokeh.plotting import figure, output_file, show
from bokeh.models import ColumnDataSource, Arrow, OpenHead, Span
from bokeh.io import curdoc
from bokeh.util.string import encode_utf8
from bokeh.resources import INLINE
from flask import Flask, render_template, redirect, url_for, request
from datetime import datetime as dt, timedelta
from burst_flow import Burst
import time
import azure_lib
import json

'''
t1 = datetime(2017, 9, 5)

t2 = t1 + timedelta(days=2)
t3 = t2 + timedelta(days=2)
'''

burst_type=28

print("Start")



def create_line(burst, plot_x_axis_min):
    print(burst.days_before)
    print(burst.days_after)

    range_start = burst.timestamp - timedelta(days = plot_x_axis_min)
    range_end = burst.timestamp - timedelta(days = burst.days_after)

    source = ColumnDataSource(burst.flow.flow_df)

    p = figure(x_axis_type="datetime",
               x_range=(range_start.timestamp()*1000, range_end.timestamp()*1000), plot_width=1300, plot_height=500)


    p.line('Timestamp', 'Flow', source=source)


    start_date = time.mktime(burst.timestamp.timetuple())*1000
    burst_start = Span(location=start_date,
                                  dimension='height', line_color='green',
                                  line_dash='dashed', line_width=2)

    p.add_layout(burst_start)
    p.yaxis.axis_label = 'Flow (L/s)'
    p.xaxis.axis_label = 'Time'
    return p

app = Flask(__name__)

'''
@app.route("/<int:bars_count>/")
def chart(bars_count):
    if bars_count <= 0:
        bars_count = 1
    return render_template("chart.html", bars_count=bars_count)
'''

@app.route('/', methods=['POST', 'GET'])
def next_burst():

    burst_sample = f'''
                    SELECT TOP(1) BURSTS.[DMA], BURSTS.[CreationDate]
                    FROM [S_INSIGHT_LEAKAGE].[BURST] BURSTS
                    LEFT JOIN [S_INSIGHT_LEAKAGE].[BURST_APP] APP
                    ON BURSTS.DMA = APP.DMA AND BURSTS.[CreationDate] = APP.[burst_detection]
                    WHERE LeakageTypeId = 28
                    AND APP.dma IS NULL
                    AND BURSTS.DMA IS NOT NULL                                
                    '''

    burst_df = azure_lib.select_sql(SQL=burst_sample)
    dma = burst_df.iloc[0]['DMA']
    creation_date = burst_df.iloc[0]['CreationDate']
    return redirect(url_for(find_burst, dma=dma, timestamp=creation_date), code=302)


@app.route('/find_burst', methods=['POST'])
def find_burst():
    burst_json = request.get_json()
    burst_dict = json.load(burst_json)

    dma = burst_dict['dma']
    timestamp = burst_dict['timestamp']

    burst1 = Burst(dma, timestamp, burst_type, 100, 3)
    print(burst1)
    plot = create_line(burst1, 30)

    # grab the static resources
    js_resources = INLINE.render_js()
    css_resources = INLINE.render_css()

    script, div = components(plot)
    html = render_template(
        'index.html',
        plot_script=script,
        plot_div=div,
        js_resources=js_resources,
        css_resources=css_resources
    )
    return encode_utf8(html)

'''
@app.route('/<dma>/<timestamp>', methods=['POST'])
def find_burst(dma, timestamp):
    #time_format = dt.strptime(timestamp, "%Y-%m-%d %H:%M:%S")
    #burst1 = Burst(dma, time_format, burst_type, 100, 3)
    burst1 = Burst(dma, timestamp, burst_type, 100, 3)
    print(burst1)
    plot = create_line(burst1, 30)

    # grab the static resources
    js_resources = INLINE.render_js()
    css_resources = INLINE.render_css()

    script, div = components(plot)
    html = render_template(
        'index.html',
        plot_script=script,
        plot_div=div,
        js_resources=js_resources,
        css_resources=css_resources
    )
    return encode_utf8(html)
'''



if __name__ == "__main__":
    app.run(debug=True)

'''
Create burst detection table:
    DMA, burst_detection, burst_actual, detection_timestamp_15, actual_timestamp_15, is clicked (initially 0) 
    string, datetime, datetime, datetime, bit

for each DMA:
    for each burst time:
        while next button is not clicked:
            replace current burst detection value

'''
