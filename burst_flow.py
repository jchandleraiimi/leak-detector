import azure_lib

class Burst:
    def __init__(self, dma, timestamp, burst_type, days_before, days_after):
        self.dma = dma
        self.timestamp = timestamp
        self.burst_type = burst_type
        self.days_before = days_before
        self.days_after = days_after
        self.flow = self.get_flow()


    def __str__(self):
        print("dma", self.dma)
        print("timestamp", self.timestamp)
        print("burst_type", self.burst_type)
        print(self.flow.flow_df)
        return "member of Test"


    def get_flow(self):

        days_before = self.days_before * -1

        benf_sample = f'''
                    SELECT [Timestamp], [Flow]
                    FROM [S_INSIGHT_LEAKAGE].[BENF]
                    WHERE dma = '{self.dma}'
                    AND [Timestamp] BETWEEN DATEADD(day, {days_before}, '{self.timestamp}') 
                    AND DATEADD(day, {self.days_after}, '{self.timestamp}')         
                '''

        benf_df = azure_lib.select_sql(SQL=benf_sample)
        return Flow(self.dma, benf_df)


class Flow:
    def __init__(self, dma, flow_df):
        self.dma = dma
        self.flow_df = flow_df
        #self.date_range = list(flow_df['timestamp'])